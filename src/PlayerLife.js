import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import CustomButton from './CustomButton';
import db from './fire';

export default class PlayerLife extends Component {
	constructor(props) {
		super(props);
		this.plusClick = this.plusClick.bind(this);
		this.minusClick = this.minusClick.bind(this);

		this.state = {
			lifeTotal: this.props.startingLifeTotal
		};

		db.settings({
			timestampsInSnapshots: true
		});
		db.collection("games").doc(this.props.gameId).collection("Players").doc(this.props.playerId)
			.onSnapshot((doc) => {
				this.setLifeTotal(doc.data().life_total)
			});
	}

	setLifeTotal(newLifeTotal) {
		this.setState((state) => {
			return { lifeTotal: newLifeTotal };
		});
	}

	plusClick() {
		var newLifeTotal = parseFloat(this.state.lifeTotal) + parseFloat(1)
		db.collection("games").doc(this.props.gameId).collection("Players")
			.doc(this.props.playerId).update({
				life_total: newLifeTotal
			})
	}

	minusClick() {
		var newLifeTotal = parseFloat(this.state.lifeTotal) - parseFloat(1)
		db.collection("games").doc(this.props.gameId).collection("Players")
			.doc(this.props.playerId).update({
				life_total: newLifeTotal
			})
	}

	render() {
		return (
			<Container>
				<Row>
					<Col>{this.props.playerId}</Col>
					<Col><CustomButton clickHandler={this.minusClick} text="-"></CustomButton></Col>
					<Col>{this.state.lifeTotal}</Col>
					<Col><CustomButton clickHandler={this.plusClick} text="+"></CustomButton></Col>
				</Row>
			</Container>
		);
	}
}