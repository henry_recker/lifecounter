import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import CustomButton from './CustomButton';
import db from './fire';
import firebase from 'firebase'

export default class PlayerLibrary extends Component {
	constructor(props) {
		super(props);
		this.addMountain = this.addMountain.bind(this);
		this.removeTopCard = this.removeTopCard.bind(this);

		this.state = {
			library: {}
		};

		db.settings({
			timestampsInSnapshots: true
		});
		
		db.collection("games").doc("game1").collection("players")
			.doc("player" + this.props.playerNumber)
				.onSnapshot((doc) => {
					this.setLibrary(doc.data().library)
		});

	}

	setLibrary(newLibrary) {
		this.setState((state) => {
			return { library: newLibrary };
		});
	}

	addMountain() {
		console.log("adding mountain")
		db.collection("games").doc("game1").collection("players")
			.doc("player" + this.props.playerNumber).update({
				library: firebase.firestore.FieldValue.arrayUnion("Mountain")
		})
	}

	removeTopCard() {
		var newLibrary = this.state.library
		if (newLibrary.length > 0) {
			newLibrary.shift()
		}
		db.collection("games").doc("game1").collection("players")
			.doc("player" + this.props.playerNumber).update({
				library: newLibrary
		})
	}

	render() {
		return (
			<div>
				<p>Total cards in library: {Object.keys(this.state.library).length}</p>
				<p>{JSON.stringify(this.state.library, null, 2)}</p>
				<Container>
					<Row>
						<Col><CustomButton clickHandler={this.addMountain} text="Add Mountain to library"></CustomButton></Col>
						<Col><CustomButton clickHandler={this.removeTopCard} text="Remove top card from library"></CustomButton></Col>
					</Row>
				</Container>
			</div>
		);
	}
}