import React, { Component } from 'react';
import db from './fire';

export default class JoinGameForm extends Component {
    constructor(props) {
        super(props);
        this.state = { gameId: '', usernames: [], username: '' };
        this.joinGame = this.joinGame.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);

        db.settings({
            timestampsInSnapshots: true
        });

        db.collection("Users").onSnapshot((snapshot) => {
            var usernameArray = []
            for (var i = 0; i < snapshot.docs.length; i++) {
                usernameArray.push(snapshot.docs[i].id)
            }

            this.setState({
                usernames: usernameArray,
                username: usernameArray[0]
            });
            this.forceUpdate();
        });
    }

    handleInputChange(event) {
        const target = event.target;
        const name = target.name;

        this.setState({
            [name]: target.value
        });
        event.preventDefault()
    }

    handleSelectChange(event) {
        const target = event.target;
        const name = target.name;

        this.setState({
            username: target.value
        });
    }

    joinGame(event) {
        //TODO for some reason the username state isn't working
        // probably need to set it at the start to give it a starting value
        console.log("joining game " + this.state.gameId + " as user with username " + this.state.username)
        event.preventDefault()
        db.collection("games").doc(this.state.gameId).collection("Players").doc(this.state.username).set({
            life_total: 40
        })
    }

    createUsernameOptions(optionValues) {
        //console.log("creating options for " + optionValues.length)
        let options = [];
        for (let i = 0; i < optionValues.length; i++) {
            /*if (i === 0) {
                this.setState({
                    username: optionValues[i]
                });
            }*/
            options.push(<option value={optionValues[i]}>{optionValues[i]}</option>);
            //here I will be creating my options dynamically based on
            //what props are currently passed to the parent component
        }
        //console.log(options)
        return options;
    }

    render() {
        return (
            <div>
                <h2>Join game by id:</h2>
                <form onSubmit={this.joinGame}>
                    <label>
                        Game id:
                <input name="gameId" type="text" value={this.state.gameId} onChange={this.handleInputChange} />
                    </label>
                    <label>
                        Select the username of the user to play as:
                        <select onChange={this.handleSelectChange}>
                            {this.createUsernameOptions(this.state.usernames)}
                        </select>
                    </label>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
}