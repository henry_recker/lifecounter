import React, { Component } from 'react';

export default class CustomButton extends Component {
  render() {
    return (
	  <button onClick={this.props.clickHandler}>{this.props.text}</button>
    );
  }
}