import React, { Component } from 'react';
import db from './fire';

export default class AddUserForm extends Component {
    constructor(props) {
        super(props);
        this.state = { userName: ''};
        this.createUser = this.createUser.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

        db.settings({
            timestampsInSnapshots: true
        });
    }

    handleInputChange(event) {
        const target = event.target;
        const name = target.name;
    
        this.setState({
          [name]: target.value
        });
      }

    createUser(event) {
        console.log("creating user with name  " + this.state.userName)
        event.preventDefault()
        db.collection("Users").add({
            username: this.state.userName
        })
    }

    render() {
        return (
            <div>
                <h2>Create new user:</h2>
                <form onSubmit={this.joinGame}>
                    <label>
                        Your username:
                <input name="userName" type="text" value={this.state.userName} onChange={this.handleInputChange} />
                    </label>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
}