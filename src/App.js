import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import PlayerLife from './PlayerLife';
//import PlayerLibrary from './PlayerLibrary';
import CustomButton from './CustomButton';
import db from './fire';
import JoinGameForm from './JoinGameForm';

class App extends Component {
  constructor(props) {
    super(props);
    this.createGame = this.createGame.bind(this);
    this.state = { messages: [], lastGameId: "-1", currentGameListener: null, users: [] }; // <- set up react state

    db.settings({
      timestampsInSnapshots: true
    });
  }
  componentWillMount() {
    /* Create reference to messages in Firebase Database */
    /*let messagesRef = fire.database().ref('messages').orderByKey().limitToLast(100);
    messagesRef.on('child_added', snapshot => {
      let message = { text: snapshot.val(), id: snapshot.key };
      this.setState({ messages: [message].concat(this.state.messages) });
    })*/
  }
  addMessage(e) {
    e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */
    /*fire.database().ref('messages').push( this.inputEl.value );*/
    this.inputEl.value = ''; // <- clear the input
  }
  createGame() {
    console.log("creating new game in firestore")
    var newID = "ERROR AHH"
    var self = this
    db.collection("games").add({
      latest_change: null,
      turn_order: null
    })
      .then(function (docRef) {
        console.log("Document written with ID: ", docRef.id);
        newID = docRef.id
        //console.log(newID)
        self.setState({ lastGameId: newID });

        if (self.state.currentGameListener) {
          self.state.currentGameListener(); //unsubscribe from old game listener
        }

        // subscribe to new game
        var currentListener = db.collection("games").doc(self.state.lastGameId).collection("Players")
          .onSnapshot((collection) => {
            //console.log('collection change')
            //console.log(collection)
            var newUsers = []
            for (let i = 0; i < collection.docs.length; i++) {
              newUsers.push(collection.docs[i].id)
            }
            console.log(newUsers)
            self.setState({ users: newUsers });
          });
        self.setState({ currentGameListener: currentListener });
        //console.log(self.state)
      })
      .catch(function (error) {
        console.error("Error adding document: ", error);
      });
  }
  createUserLifeCounters(users) {
    let lifeCounters = [];
    for (let i = 0; i < users.length; i++) {
      lifeCounters.push(<PlayerLife gameId={this.state.lastGameId} playerId={users[i]}/>);
    }
    return lifeCounters;
  }
  render() {
    return (
      <Container>
        <CustomButton clickHandler={this.createGame} text="Create a new game in firestore"></CustomButton>
        <p>Last created game id: {this.state.lastGameId}</p>
        {/*<Row>
          <Col>
            <PlayerLife playerNumber="1" name="1" startingLifeTotal="5" />
            <PlayerLibrary playerNumber="1"></PlayerLibrary>
          </Col>
          <Col>
            <PlayerLife playerNumber="2" name="2" startingLifeTotal="15" />
            <PlayerLibrary playerNumber="2"></PlayerLibrary>
          </Col>
        </Row>
        <Row>
          <Col>
            <PlayerLife playerNumber="3" name="3" startingLifeTotal="5" />
            <PlayerLibrary playerNumber="3"></PlayerLibrary>
          </Col>
          <Col>
            <PlayerLife playerNumber="4" name="4" startingLifeTotal="15" />
            <PlayerLibrary playerNumber="4"></PlayerLibrary>
          </Col>
        </Row>*/}
        <JoinGameForm />
        <Row>
          {this.createUserLifeCounters(this.state.users)}
        </Row>
      </Container>
    );
  }
}

export default App;